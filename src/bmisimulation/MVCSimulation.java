/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmisimulation;

public class MVCSimulation {
    
    public static void main(String[] args){
        
        Student model = retrieveStudentFromDatabase();
        
        StudentView view = new StudentView();
        
        StudentController controller = new StudentController(model, view);
        
        controller.updateView();
    }
    
    public static Student retrieveStudentFromDatabase(){
        Student student = new Student("Larry", "10");
        return student;
    }
}
