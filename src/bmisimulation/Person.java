/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmisimulation;

public class Person {
    
    private String name;
    private double weight;
    private double height;
    private Weight unitWeight;
    private Height unitHeight;
    
    Person(){}
    
    public Person(String name){
        this.name = name;
    }
    
    public Person(String name, double weight, Weight unitWeight, double height, Height unitHeight ){
        this.name = name;
        this.weight = weight;
        this.unitWeight = unitWeight;
        this.height = height;
        this.unitHeight = unitHeight;
    }
    
    public String getName(){
        return name;
    }
    
     public double getWeight(){
        return weight;
    }
    
    public double getHeight(){
        return height;
    }
    
    public Weight getUnitWeight(){
        return unitWeight;
    }
    
    public Height getUnitHeight(){
        return unitHeight;
    }
    
    public double getBMI(){
        
        double weightInKilos = 0;
        double heightInMetres = 0;
        double bmi = 0;
        
        switch(unitWeight) {
            case KG : weightInKilos = weight;
            break;

            case LB : weightInKilos = weight / 2.205; 
            break;
        }
        
         switch(unitHeight) {
            case IN : heightInMetres = height / 39.37; 
            break;

            case M : heightInMetres = height; 
            break;
        }
            
        bmi = (weightInKilos / (Math.pow(heightInMetres,2))); 
            
                return bmi;

        } 
        
        
    }
    

