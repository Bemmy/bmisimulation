/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmisimulation;

import java.util.*;

public class BMISimulation {

    public static void main(String[] args) {
            
        Person person1 = new Person("Francis", 82, Weight.KG, 1.72, Height.M);
        
        Person person2 = new Person("Allison", 152, Weight.LB, 70, Height.IN);

        List<Person> people = new  ArrayList<>();
        people.add(person1);
        people.add(person2);
        
        for(Person p : people){
            System.out.printf("Name: %s BMI: %.2f\n", p.getName(), p.getBMI());
        }
    }
    
}
